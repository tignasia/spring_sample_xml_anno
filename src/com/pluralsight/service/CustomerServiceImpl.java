package com.pluralsight.service;

import java.util.List;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pluralsight.model.Customer;
import com.pluralsight.repository.CustomerRepository;
import com.pluralsight.repository.HibernateCustomerRepositoryImpl;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Service("customerService")
public class CustomerServiceImpl implements CustomerService, DisposableBean {

	private CustomerRepository customerRepository;
	
	public CustomerServiceImpl() {
		
	}

	@PostConstruct
	public void postConstruct() {
		System.out.println("postConstruct !!!");
	}

	@PreDestroy
	public void preDestroy() {
		System.out.println("preDestroy !!!");

	}

	public CustomerServiceImpl(CustomerRepository customerRepository) {
		System.out.println("We are using constructor injection");
		this.customerRepository = customerRepository;
	}
	
	@Autowired
	public void setCustomerRepository(CustomerRepository customerRepository) {
		System.out.println("We are using setter injection");
		
		this.customerRepository = customerRepository;
	}

	public List<Customer> findAll() {
		return customerRepository.findAll();
	}

	public void destroy() throws Exception {
		System.out.println("destroy !!!");
	}
	

}
